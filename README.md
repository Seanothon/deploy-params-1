# Deployment Tool #

This tool was originally developed to determine changes occuring in a codebase hosted on a repo and deploy them to a saleforce organization. 

[Jenkins Build Dashboard](http://10.229.211.100:8080/view/Builds%20-%20Code%20Deploy/) (Accessible from company's network only)

### Existing features ###

* Pull Jenkins parameters
* Perform git diff between previous and current version of codebase
* Pull SF org credentials and specific parameters from mySQL db
* Modify specific files with corresponding parameters (site admin and subdomain on SF community is an example with lob-community)
* Build deployment package containing only the changes
* Perform ant deployment of the generated package to SF org
* Monitor the deployment and cancel&restart if pending for too long
* Build HTML report once the deployment is done
* Save progress to mySQL db (commit hash)

### Prerequisites ###

Applications needed on the machine running the deployment tool

* npm
* Ant and the Force.com Migration Tool ( A Jar File Found [Here](https://developer.salesforce.com/page/Force.com_Migration_Tool), needs to be downloaded and saved in ~/.ssh/lib on Macs)
* Repository must have a directory containing the Salesforce code and the package.xml (usually src/ folder but parameter can be changed under _targets_, _dirPath = src/package.xml_)
* Repository must have a directory containing the Salesforce dependencies and the package.xml (usually dependencies/ folder but parameter can be changed under _targets_, _dirPath = dependencies/package.xml_)

_Note: Repository setting up can be different but just needs to match parameters set in **params.json** under **targets**._

### How do I get set up? ###

Install through npm in ANY local repository ...

	cd directory/path/repository
	npm install https://username@bitbucket.org/liveoakbank/deploy-params.git#master

Need to change these parameters in **params.json** to run it locally:

* set _jenkinsJob_ to false
* _sfOrg_ = SF Org name as it is set in the mySQL db OR leave empty if credentials are local
* _sfUsername_ = SF username, example@liveoakbank.com.pqa3
* _sfPassword_ = SF password + security token
* _sfServerUrl_ = https://test.salesforce.com OR https://login.salesforce.com
* _gitBranch_ isn't currently used, leave blank
* _pendingCountMax_ = 5, means the monitoring will cancel the deployment after 5 pending rows
* _buildMonitorInterval_ = 5000, means the deployment state is monitored every 5000ms
* _buildMaxRetries_ = 10, means the monitoring will cancel and restart the deployment until it does not time out OR the build will fail after 10 attempts

Given that this tool deploys only the modified files, here are the parameters to edit in order to either **BLOCK or FORCE deployment of all files**. Under **targets** in the **params.json**, there are 2 different deployment items: one with _antDeployCommand = deployDependencies_ for nCino managed packages dependencies and the other with _antDeployCommand = deployCode_ for the whole codebase contained in the _src/_ folder. 
For each of these 2 deployment items:

* set _bypassChanges_ to true in order to FORCE deployment of all
* set _blockDeploy_ to true in order to BLOCK deployment of all

### How do I run it? ###

Run this command in terminal to run it locally:

	npm run --prefix node_modules/deploy-params deploy

Also can set the local package.json of your repositosy as follows to add a shortcut:

	"scripts": {
		"deploy": "node node_modules/deploy-params/js/update-deploy-params.js"
	}
	
Then use:

	npm run deploy

Find HTML reports under: /node_modules/deploy-params/report
