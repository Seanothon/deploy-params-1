//Created by Caleb Duckwall
var exports         = module.exports = {},
    loginDataObject = require('../loginData.json'),
    mysql           = require('mysql');

exports.queryBuildData = function query(org, callback) {
    
    var connection = mysql.createConnection({
        host     : loginDataObject.MySQL.url,
        user     : loginDataObject.MySQL.login,
        password : loginDataObject.MySQL.password
    });
    
    connection.query(
        "SELECT * FROM qa_automation.builds " +
        "INNER JOIN qa_automation.login_cred " +
        "ON qa_automation.builds.login_cred_Id = qa_automation.login_cred.idQA_Automation " +
        "WHERE qa_automation.builds.org = '" + org + "'", function(error, results, fields){

        if (error) {
            console.error(error.stack);
        }
        else {
            callback(results);
        }

    });

    connection.end();
}

exports.updateLatestCommitData = function query(org, latest_commit_pushed, callback) {

    var connection = mysql.createConnection({
        host     : loginDataObject.MySQL.url,
        user     : loginDataObject.MySQL.login,
        password : loginDataObject.MySQL.password
    });

    connection.query(
        "UPDATE qa_automation.builds " +
        "SET qa_automation.builds.latest_commit_pushed = '" + latest_commit_pushed + "' " +
        "WHERE qa_automation.builds.org = '" + org + "'", function(error, results, fields){

        if (error) {
            console.error(error.stack);
        }
        else {
            callback(results);
        }
    });

    connection.end();
}