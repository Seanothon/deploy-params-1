// Created by Celio Nelson
// This grabs parameters from Jenkins job and modifies xml files listed in the model.
// 

// Import modules
var fs = require('fs'),
    xml2js = require('xml2js'),
    shell = require('shelljs'),
    map = require('../sf-correspondance.json'),
    model = require('../params.json'),
    mysqldb = require('./mysql.js');

var octopus = {

	init: function() {
		var self = this;

		/*****/
		/**/self.log.debug('Entering init');
		/*****/
		
		self.insertJenkinsParams(function() {

			/*****/
			/**/self.log.debug('insertJenkinsParams function done.');
			/*****/

			// self.gitCheckoutBranch(function() {

			// 	/*****/
			// 	/**/self.log.debug('gitCheckoutBranch function done.');
			// 	/*****/

				self.setGitDiff(function() {

					/*****/
					/**/self.log.debug('setGitDiff function done.');
					/*****/

					self.performIFTTTActions(function() {

						/*****/
						/**/self.log.debug('performIFTTTActions function done.');
						/*****/

						self.setRepoChanges(function() {

							/*****/
							/**/self.log.debug('setRepoChanges function done.');
							/*****/

							self.setDeployDirectory(function() {

								/*****/
								/**/self.log.debug('setDeployDirectory function done.');
								/*****/

								self.updateXML(function() {

									/*****/
									/**/self.log.debug('updateXML function done.');
									/*****/

									self.deploy(0, 0, function() {

										/*****/
										/**/self.log.debug('deploy function done.');
										/*****/

										self.setGitHead(function() {

											/*****/
											/**/self.log.debug(
											/**/	'setGitHead function done. *** \n' +
											/**/	'init function done. ***'
											/**/);
											/*****/

											self.build.complete();
										});
									});
								});
							});
						});
					});	
				});
			// });
		});
	},

	build: {

		complete: function() {

			shell.exit(0);
		},

		fail: function(message, error) {

			if (message || error) {
				console.error(
					'*** ERROR ***\n' +
					'\n' + message +
					'\n' + error +
					'\n*************'
				);
			}
			shell.exit(1);
		}
	},

	log: {

		getDebugState: function() {

			return model.log.debug;
		},

		getPackagingInstructionsState: function() {

			return model.log.packagingInstructions;
		},

		debug: function(debugString) {

			var showDebug = octopus.log.getDebugState();

			if (showDebug) {

				console.log(
					'******' +
					'\n' + debugString +
					'\n******'
				);
			}
		},

		packagingInstructions: function(instructionsString) {

			var showInstructions = octopus.log.getPackagingInstructionsState();

			if (showInstructions) {

				console.log(instructionsString);
			}
		},

		antiPackagingInstructions: function(antiInstructionsString) {

			var showAntiInstructions = !octopus.log.getPackagingInstructionsState();

			if (showAntiInstructions) {

				console.log(antiInstructionsString);
			}
		}
	},

	getTargets: function() {

		return model.targets;
	},

	getDeploySettings: function() {

		return model.deploySettings;
	},

	getGlobalPaths: function() {

		return model.globalPaths;
	},

	getReport: function(reportName) {

		return model.reports[reportName];
	},

	getPreDeployIFTTT: function() {

		return model.preDeployIFTTT;
	},

	replaceAll: function(string, toReplace, replaceWith) {

		var result,
			toUpdate;

		for (i = 0; i < toReplace.length; i++) {

			if(result) {

				toUpdate = result;
			} else {

				toUpdate = string;
			}

			result = toUpdate.replace(new RegExp(toReplace[i], 'g'), replaceWith[i]);
		}

		return result;
	},

	convertBoolVar: function(param) {

		if (param == 'true') {

			return true;
		} else if (param == 'false') {

			return false;
		} else {

			return undefined;
		}
	},

	maskString: function(str) {

		var res;

		res = str.charAt(0);

		for (i = 1; i < (str.length - 1); i++)	{

			res += '*';
		}

		res += str.charAt(str.length - 1);

		return res;
	},

	convertPicklistMatch: function(param, toMatch) {

		if (typeof toMatch != 'string') {

			if (toMatch.includes(param)) {

				return true;
			} else {

				return false;
			}
		} else {

			if (param == toMatch) {

				return true;
			} else {

				return false;
			}
		}
	},

	insertJenkinsParams: function(callback) {

		var self = this;

		if(model.jenkinsJob) {

			model.log.debug = self.convertPicklistMatch(process.env.DEBUG, ['Debug', 'All', 'true']);
			model.log.packagingInstructions = self.convertPicklistMatch(process.env.DEBUG, ['Packaging Instructions', 'All']);
			model.deploySettings.sfOrg = process.env.ORG;
			model.deploySettings.sfUsername = process.env.USERNAME;
			model.deploySettings.sfPassword = process.env.PASSWORD;
			model.deploySettings.sfServerUrl = process.env.SERVERURL;
			model.deploySettings.gitBranch = process.env.BRANCH;
			model.deploySettings.pendingCountMax = process.env.MAX_PENDING;
			model.deploySettings.buildMaxRetries = process.env.MAX_BUILD_RETRIES;
			model.targets[0].changes[0].value = process.env.SITE_ADMIN;
			model.targets[0].changes[1].value = process.env.SUB_DOMAIN;
			model.targets[1].bypassChanges = self.convertPicklistMatch(process.env.DEPLOY_DEPENDENCIES_MODE, 'Force');
			model.targets[2].bypassChanges = self.convertPicklistMatch(process.env.DEPLOY_CODE_MODE, 'Force');
			model.targets[1].blockDeploy = self.convertPicklistMatch(process.env.DEPLOY_DEPENDENCIES_MODE, 'Block');
			model.targets[2].blockDeploy = self.convertPicklistMatch(process.env.DEPLOY_CODE_MODE, 'Block');
			model.preDeployIFTTT[0].blockAction = self.convertPicklistMatch(process.env.DEPLOY_CC_TYPESCRIPTS, 'Block');
			model.preDeployIFTTT[0].bypassChanges = self.convertPicklistMatch(process.env.DEPLOY_CC_TYPESCRIPTS, 'Force');
			model.globalPaths.commitRefDir = process.env.COMMIT_REF_DIRECTORY;
			model.globalPaths.commitRef = process.env.COMMIT_REF;

			if(self.convertBoolVar(process.env.RUN_CHECK_ONLY)) {

				model.targets[2].antDeployCommand = '-Dsf.checkOnly="true" ' + model.targets[2].antDeployCommand;
			}

			if(self.convertBoolVar(process.env.RUN_LOCAL_TESTS)) {

				model.targets[2].antDeployCommand = '-Dsf.testLevel="RunLocalTests" ' + model.targets[2].antDeployCommand;
			}
		}

		if (model.deploySettings.sfOrg) {

			mysqldb.queryBuildData(model.deploySettings.sfOrg, function(results) {

				/*****/
				/**/self.log.debug('mySQL DB = ' + JSON.stringify(results));
				/*****/

				if (results.length < 1) {

					self.build.fail('****** Did not find ORG in mySQL database ******');
				} else {

					model.deploySettings.sfUsername = results[0].username;
					model.deploySettings.sfPassword = results[0].password + results[0].security_token;
					model.deploySettings.sfServerUrl = results[0].url;
					model.deploySettings.gitBranch = results[0].branch;
					model.deploySettings.pendingCountMax = results[0].max_pending_lines;
					model.deploySettings.buildMaxRetries = results[0].max_build_retries;
					model.targets[0].changes[0].value = results[0].community_site_admin;
					model.targets[0].changes[1].value = results[0].community_sub_domain;

					if (results[0].latest_commit_pushed && !model.globalPaths.commitRef) {

						model.globalPaths.commitRef = results[0].latest_commit_pushed;
					}
				}

				/*****/
				/**/self.log.debug('Model = ' + JSON.stringify(model));
				/*****/	

				callback();				
			});
		} else {

			/*****/
			/**/self.log.debug('Model = ' + JSON.stringify(model));
			/*****/

			callback();
		}	
	},

	updateXML: function(callback) {

		var self = this;

		/*****/
		/**/self.log.debug('Entering updateXML');
		/*****/

		var targets = self.getTargets(),
			paths = self.getGlobalPaths();

		for (i = 0; i < targets.length; i++) {

			/*****/
			/**/self.log.debug('i (targets) = ' + i);
			/*****/

	    	var parser = new xml2js.Parser(),
	    		target = targets[i];

	    	try {

	    		if (shell.test('-f', paths.deployDir + target.dirPath) && !target.bypassChanges) {

					data = fs.readFileSync((paths.deployDir + target.dirPath));
				} else {

					continue;
				}

			} catch (err) {

				self.build.fail('Not able to read ' + paths.deployDir + target.dirPath + ' because of the following:', err.stack);
			}

			// Parses XML and converts it in JSON
		    parser.parseString(data, function (err, result) {
		    	var res;

		    	// Updates JSON with parameters.
		    	for (j = 0; j < target.changes.length; j++) {
			    	result = JSON.parse(JSON.stringify(result), function(k, v) {
			    		
			    		if (k == target.changes[j].xmlPath && target.changes[j].value.length > 0) {
			    			return target.changes[j].value;
			    		}
			    		return v;
			    	})
			    }

			    // Converts JSON back to XML and updates the target file. 
		        var builder = new xml2js.Builder();
				var xml = builder.buildObject(result);

				try {

					fs.writeFileSync((paths.deployDir + target.dirPath), xml)

					/*****/
					/**/self.log.debug(
					/**/	paths.deployDir + target.dirPath + ' updated with following changes.' +
					/**/	'\n' + xml
					/**/);
					/*****/

				} catch (err) {

					self.build.fail('Not able to write to ' + paths.deployDir + target.dirPath + ' because of the following:', err.stack);
				}
		    });	
		}

		/*****/
		/**/self.log.debug('Exiting updateXML');
		/*****/

		callback();
	},

	setRepoChanges: function(callback) {
		
		var self = this;

		/*****/
		/**/self.log.debug('Entering setRepoChanges');
		/*****/

		var targets = self.getTargets(),
			paths = self.getGlobalPaths();

		try {

			var content = fs.readFileSync(paths.diffsDir + 'diff','utf8'),
				changes;
		} catch (err) {

			self.build.fail('Not able to set repo changes because of the following:', err.stack);
		}

		// The 'changes' variable now contains an array of filesPaths of all files that were modified.
		changes = content.split('\n');

		/*****/
		/**/self.log.debug('Content = ' + content);
		/*****/

		/*****/
		/**/self.log.debug('Changes after split = ' + changes);
		/*****/

		for (i = 0; i < targets.length; i++) {

			/*****/
			/**/self.log.debug('i (targets) = ' + i);
			/*****/

			// We don't need this function if needsRepoUpdate is false. 
			if (!targets[i].needsRepoUpdate || targets[i].bypassChanges) {
				
				/*****/
				/**/self.log.debug('Skipped this i');
				/*****/

				continue;
			}

			/*****/
			/**/self.log.debug('Current directory = ' + shell.pwd());
			/*****/

			var result = {},
				filesPaths = [],
				src = targets[i].dirPath.split('/');

			for (j = 0; j < changes.length; j++) {
				var pathToFile = changes[j];
				var tmp = pathToFile.split('/');

				/*****/
				/**/self.log.debug('tmp = ' + JSON.stringify(tmp));
				/*****/

				/*****/
				/**/self.log.debug('src = ' + JSON.stringify(src));
				/*****/

				// Filtering, only taking into account files contained in src folder.
				if (tmp[0] == src[0]) {

					var mapped = map[tmp[1]];

					// Again filtering, now only processing types that are listed in sf-correspondance.json.
					if (mapped) {

						if (mapped.hasSubFolders) {
							if (tmp.length == 4) {

								p = tmp[2] + '/' + tmp[3];
							} else {

								p = tmp[2].replace('-meta.xml', '');
							}
						} else {

							p = tmp[2];
						}

						// Add type if not already contained in result json.
						if (!result[mapped.typeName]) {

							result[mapped.typeName] = [];
						}

						var l = p.indexOf(mapped.extension),
							res = '';

						// If the extension isn't specified in sf-correspondance (*) AND the extension needs to be truncated in the result json, we can't determine where to truncate => WARNING.
						if (mapped.extension == '*' && !mapped.keepExtension) {
							self.build.fail('CANNOT HAVE UNSPECIFIED EXTENSION. => ' + p, '');
						}

						// If the extension is matched OR the extension does not need to be truncated, AND if the file or folder was not added to the result json yet, add it now.
						if (((l == -1) || mapped.keepExtension) && (result[mapped.typeName].indexOf(p) == -1)) {

							// Extension not truncated.
							res = p;
						} else {

							// Extension truncated.
							res = p.substr(0, (l - 1));
						}

						// Excluding all .xml (meta) files, removing extension.
						res = res.replace('-meta.xml', '');

						// Making sure not to add empty result or duplicate.
						if (res != '' && !result[mapped.typeName].includes(res)) {

							// Adding to result json.
							result[mapped.typeName].push(res);
						}
					} else if (tmp[1] != 'package.xml') {

						// TO DO: ADD WARNING TYPE NOT IN CORRESPONDANCE
						console.log('WARNING, TYPE NOT IN CORRESPONDANCE, NOT ADDED TO PACKAGE => ' + tmp[1]);
					}

					if (mapped || tmp[1] == 'package.xml') {

						// Adding file paths.
						filesPaths.push(pathToFile);
					}
				}
			}

			/*****/
			/**/self.log.debug('filesPaths (before push to model) = ' + filesPaths);
			/*****/

			model.targets[i].filesPaths = filesPaths;

			/*****/
			/**/self.log.debug('Result (before push to model) = ' + JSON.stringify(result));
			/*****/
			
			var keys = Object.keys(result);

			if (keys.length > 0) {

				for (m = 0; m < keys.length; m++) {
					
					var obj = {
						'members': [],
						'name': keys[m]
						};

					for (n = 0; n < result[obj.name].length; n++) {

						obj.members.push(result[obj.name][n]);
					}

					model.targets[i].changes[0].value.push(obj);

					/*****/
					/**/self.log.debug('model.targets[' + i + '].changes[0].value = ' + JSON.stringify(model.targets[i].changes[0].value));
					/*****/
				}
			}
		}

		/*****/
		/**/self.log.debug('Exiting setRepoChanges');
		/*****/

		callback();
	},

	performGit: function(gitCommand) {

		var self = this;

		var paths = self.getGlobalPaths();

		var command = 'git --git-dir=' + paths.pathToRepo + '.git/ --work-tree=' + paths.pathToRepo + ' ' + gitCommand;

		/******/
		/**/self.log.debug(
		/**/	'Git operation is:' +
		/**/	'\n' + command
		/**/); 
		/******/

		try {
			
			// Adding this since exit code 128 is not caught by the try/catch
			if(shell.exec(command).code !== 0) {

				self.build.fail('Unhandled failure with following git operation:', command);
			}		
		} catch (err) {

			self.build.fail('Not able to perform GIT operation because of the following:', command + '/n' + err.stack);
		}
	},

	performDiff2Html: function(gitCommand, reportName) {

		var self = this;

		var paths = self.getGlobalPaths(),
			report = self.getReport(reportName),
			template = '',
			htmlReport = '',
			htmlReportUpdated = '';
			
		var command = 'diff2html --file ' + report.htmlFilePath + ' --style "side" ' + '--summary "' + report.filesSummary + '" -- ' + gitCommand + ' -- --git-dir=' + paths.pathToRepo + '.git/ --work-tree=' + paths.pathToRepo + ' > null';

		
		/******/
		/**/self.log.debug(
		/**/	'diff2html operation is:' +
		/**/	'\n' + command
		/**/); 
		/******/

		try {
			
			// Adding this since exit code 128 is not caught by the try/catch
			if(shell.exec(command).code !== 0) {

				self.build.fail('Unhandled failure with following diff2html operation:', command);
			}		

			if (shell.test('-f', report.htmlFilePath)) {
				template = fs.readFileSync(report.templatePath, 'utf8');
				htmlReport = fs.readFileSync(report.htmlFilePath, 'utf8');

				/******/
				/**/self.log.debug(
				/**/	'template = ' + template +
				/**/	'\nhtmlReport size = ' + htmlReport.length
				/**/);
				/******/

				htmlReport = htmlReport.toString();

				/******/
				/**/self.log.debug(
				/**/	'htmlReport size = ' + htmlReport.length
				/**/);
				/******/

				htmlReportUpdated = htmlReport.replace('<h1>Diff to HTML by <a href="https://github.com/rtfpessoa">rtfpessoa</a></h1>', template);

				fs.writeFileSync(report.htmlFilePath, htmlReportUpdated);

			} else {

				console.log('*** No changes for report "' + reportName + '" ***\n\n');
				template = fs.readFileSync('report/template/template-no-change', 'utf8');
				fs.writeFileSync(report.htmlFilePath, template);
			}

		} catch (err) {

			self.build.fail('Not able to perform diff2html operation because of the following:', command + '\n' + err.stack);
		}
	},

	gitCheckoutBranch: function(callback) {

		var self = this;

		var deploySettings = self.getDeploySettings(),
			paths = self.getGlobalPaths();

		console.log(
			'***' +
			'\ngit checkout ' + deploySettings.gitBranch
		);
		self.performGit('checkout ' + deploySettings.gitBranch);
		
		console.log(
			'***' +
			'\ngit pull'
		);
		self.performGit('pull');	

		callback();
	},

	getNonUpgradables: function(isExcluded) {

		var self = this;

		var gitString = ''
			paths = self.getGlobalPaths();

		for (var key in map) {

			if (!map[key].upgradable) {

				if(isExcluded) {

					gitString += '":(exclude)' + paths.pathToRepo + 'src/' + key + '/*" ';
				} else {

					gitString += '"' + paths.pathToRepo + 'src/' + key + '/*" ';
				}
			}
		}

		return gitString;
	},

	setGitDiff: function(callback) {
		
		var self = this;

		/*****/
		/**/self.log.debug('Entering setGitDiff');
		/*****/

		var targets = self.getTargets(),
			paths = self.getGlobalPaths();

		// Removing remaining diffs from last run.
		try {

			shell.rm('-rf', paths.diffsDir + '*');
		} catch (err) {

			self.build.fail('Not able to empty ' + paths.diffsDir + ' directory because of the following:', err.stack);
		}

		var head = {};

		/*****/
		/**/self.log.debug(
		/**/	'headDir = ' + paths.commitRefDir + 
		/**/	'\nCurrent directory = ' + shell.pwd() + 
		/**/	'\npaths.commitRef = ' + paths.commitRef
		/**/);
		/*****/

		if (paths.commitRef) {

			head.old = paths.commitRef;

		} else if (shell.test('-d', paths.commitRefDir) && shell.test('-f', paths.commitRefDir + 'HEAD')) {

			head.old = fs.readFileSync(paths.commitRefDir + 'HEAD','utf8');
		}

		if (shell.test('-f', paths.pathToRepo + '.git/HEAD')) {
			head.new = fs.readFileSync(paths.pathToRepo + '.git/HEAD','utf8');
		}

		/*****/
		/**/self.log.debug('head = ' + JSON.stringify(head));
		/*****/

		if (head.new && head.old) {
			head.old = head.old.split('\n');
			head.old = head.old[0];
		
			if (head.old.includes('ref:')) {

				var pathToOldHead = paths.commitRefDir + head.old.substr(5);

				try {

					head.old = fs.readFileSync(pathToOldHead,'utf8');
				} catch (err) {

					self.build.fail('Not able to read ' + pathToOldHead + ' because of the following:', err.stack);
				}

				head.old = head.old.split('\n');
				head.old = head.old[0];
			}

			head.new = head.new.split('\n');
			head.new = head.new[0];
		
			if (head.new.includes('ref:')) {

				var pathToNewHead = paths.pathToRepo + '.git/' + head.new.substr(5);

				try {

					head.new = fs.readFileSync(pathToNewHead,'utf8');
				} catch (err) {

					self.build.fail('Not able to read ' + pathToNewHead + ' because of the following:', err.stack);
				}

				head.new = head.new.split('\n');
				head.new = head.new[0];
			}

			/*****/
			/**/self.log.debug('Current directory before git command = ' + shell.pwd());
			/*****/

			//TEMPORARY FIX, NEED TO REFACTOR THIS (TK-2555).
			model.globalPaths.commitRef = head;

			if (head.old != head.new) {
				// Performing a git diff to get the latest changes only.

				console.log(
					'******' +
					'\n*** Comparing between Commits:' +
					'\n*** Previous: ' + head.old +
					'\n*** Current: ' + head.new
				);
				console.log(
					'*** Getting the following changes (A for Added, M for Modified, D for Deleted) ***' +
					'\n*** Note: Deletions are ignored by the automated process ***'
				);
				
				self.log.antiPackagingInstructions(
					'*** MORE DETAIL IN HTML REPORT ***'
				);

				self.log.packagingInstructions(
					'*** SEE HTML REPORT CONTAINING NOTES FOR DEPLOYMENTS TO PACKAGING ORG ***'
				);

				self.performGit('diff --name-status ' + head.old + ' ' + head.new);
				console.log('...');

				// Generate report for regular deployment.
				if(!self.log.getPackagingInstructionsState()) {
					
					self.performDiff2Html(head.old + ' ' + head.new + ' -- "' + paths.pathToRepo + 'src/*"', 'all');
				}

				// Generate report for deployment to packaging org.
				if(self.log.getPackagingInstructionsState()) {
					// Diff gives list of ALL but DELETED and MODIFIED
					self.performDiff2Html('--diff-filter=dm ' + head.old + ' ' + head.new + ' -- ' + paths.pathToRepo + 'src/ ":(exclude)' + paths.pathToRepo + 'src/*.xml"', 'added-renamed');
					
					// Diff gives list of NON-UPGRADABE MODIFIED only
					self.performDiff2Html('--diff-filter=M ' + head.old + ' ' + head.new + ' -w -- ' + self.getNonUpgradables(false) + '":(exclude)' + paths.pathToRepo + 'src/*.xml"', 'non-upgradable-modified');
					
					// Diff gives list of DELETED only
					self.performDiff2Html('--diff-filter=D ' + head.old + ' ' + head.new + ' -- ' + paths.pathToRepo + 'src/ ":(exclude)' + paths.pathToRepo + 'src/*.xml"', 'deleted');
					
					// Detailed diff of OBJECTS only
					self.performDiff2Html('--diff-filter=MA ' + head.old + ' ' + head.new + ' -w -- "' + paths.pathToRepo + 'src/objects/*"' + ' ":(exclude)' + paths.pathToRepo + 'src/*.xml"', 'objects');
				
					// Diff gives list of OTHER MODIFIED only
					self.performDiff2Html('--diff-filter=M ' + head.old + ' ' + head.new + ' -- ' + paths.pathToRepo + 'src/ ' + self.getNonUpgradables(true) + '":(exclude)' + paths.pathToRepo + 'src/*.xml"', 'upgradable-modified');
				
					// Diff gives list of XML changes only
					self.performDiff2Html(head.old + ' ' + head.new + ' -- "' + paths.pathToRepo + 'src/*.xml"', 'xml-changes');
				}


				// Write the FULL diff to diff file to build the deployment package.
				self.performGit('diff --diff-filter=d --name-only ' + head.old + ' ' + head.new + ' > ' + paths.diffsDir + 'diff');
			} else {

				console.log(
					'******' + 
					'\n*** No difference, same commit.');
			}

		} else if (!targets[2].bypassChanges) {

			self.build.fail('****** \n' +
                '*** SOMETHING WENT WRONG WITH THE GIT HEADS, THROWING ERROR TO CATCH THIS. \n' +
				'*** Please let Celio know for debugging \n' +
				'*** If urgent, to manually bypass this error, change parameter "DEPLOY_CODE_MODE" to "Force". \n' +
                '*** head.old = ' + head.old + '\n' +
                '*** head.new = ' + head.new + '\n' +
                '******', ''
			);
		} else {

            console.log('****** \n' +
                '*** SOMETHING WENT WRONG WITH THE GIT HEADS. \n' +
                '*** Please let Celio know for debugging \n' +
                '*** Forcing deploy of code anyway. \n' +
                '*** head.old = ' + head.old + '\n' +
                '*** head.new = ' + head.new + '\n' +
                '******'
            );
		}

		if (!shell.test('-f', paths.diffsDir + 'diff')) {

			/*****/
			/**/self.log.debug('diff file contains no differences');
			/*****/
			shell.exec('echo "null"> ' + paths.diffsDir + 'diff');
		}

		/*****/
		/**/self.log.debug('Exiting setGitDiff');
		/*****/

		callback();
	},

	performIFTTTActions: function(callback) {

		var self = this;

		/*****/
		/**/self.log.debug('Entering performIFTTTActions');
		/*****/

		var paths = self.getGlobalPaths(),
			actions = self.getPreDeployIFTTT(),
			act,
			tt,
			sh;

		if (actions) {

			for (i = 0; i < actions.length; i++) {

				act = actions[i];

				console.log(
					'******' +
					'\n*** IFTTT_ACTION_FORCED = ' + act.bypassChanges + ' for ' + act.name + ' ***' +
					'\n*** IFTTT_ACTION_BLOCKED = ' + act.blockAction + ' for ' + act.name + ' ***'
				);

				if (act.blockAction) {

					continue;
				}

				/*****/
				/**/self.log.debug('act.ifThis = ' + act.ifThis);
				/*****/			
					
				act.ifThis = self.replaceAll(act.ifThis, ['%pathToRepo%', '%oldHead%', '%newHead%'] , [paths.pathToRepo, paths.commitRef.old, paths.commitRef.new]); //Need fix if TK-2555 changes.

				/*****/
				/**/self.log.debug('act.ifThis after replaceAll = ' + act.ifThis);
				/*****/

				try {

					sh = shell.exec(act.ifThis, {silent:true});

					/*****/
					/**/self.log.debug('Output of command "' + act.ifThis + '" : \n' + sh.stdout);
					/*****/

					if (sh.code !== 0) {

						self.build.fail(act.failureMessage + ' with "' + act.ifThis + '" command because of the following:', sh.stderr);
					}
				} catch(err) {

					self.build.fail(act.failureMessage + ' with "' + act.ifThis + '" command because of the following:', err.stack);					
				}

				if (shell.exec(act.ifThis, {silent:true}).stdout != '' || act.bypassChanges) {

					console.log(
						'*** ' + act.inProgressMessage +
						'\n******'
					);

					for (j = 0; j < act.thenThese.length; j++) {

						tt = act.thenThese[j];

						tt = self.replaceAll(tt, ['%pathToRepo%', '%newHead%', '%diffsDir%'] , [paths.pathToRepo, paths.commitRef.new, paths.diffsDir]);

						/*****/
						/**/self.log.debug(
						/**/	'Current directory = ' + shell.pwd() +
						/**/	'\nIFTTT command = ' + tt
						/**/);
						/*****/

						try {
							// If the command is 'cd', we need to use shelljs' special command
							if(tt.substring(0, 2) == 'cd') {

								sh = shell.cd(tt.substring(3)); 
							} else {

								sh = shell.exec(tt, {silent: true});
							}

							if (sh.code !== 0) {

								self.build.fail(act.failureMessage + ' with "' + tt + '" command because of the following:', sh.stderr);
							}
						} catch (err) {

							self.build.fail(act.failureMessage + ' with "' + tt + '" command because of the following:', err.stack);
						}
					}
				} else {

					console.log(
						'*** ' + act.noChangesMessage +
						'\n******'
					);
				}
			}
		}

		callback();
	},

	setDeployDirectory: function(callback) {

		var self = this;

		/*****/
		/**/self.log.debug('Entering setDeployDirectory');
		/*****/

		var paths = self.getGlobalPaths(),
			targets = self.getTargets();

		try {

			// Creating the folder if not here yet.
			if (!shell.test('-d', paths.deployDir)) {

				/*****/
				/**/self.log.debug('Making directory because inexistant.');
				/*****/

				shell.mkdir('-p', paths.deployDir);
			}

			// Removing remaining HEAD from last run.
			shell.rm('-rf', paths.deployDir + '*');

			shell.cp('-r', paths.pathToRepo + 'build.properties', paths.deployDir);
			shell.cp('-r', paths.pathToRepo + 'build.xml', paths.deployDir);
		} catch (err) {

			self.build.fail('Not able to perform operations from ' + paths.pathToRepo + ' to ' + paths.deployDir + ' because of the following:', err.stack);
		}

		for (i = 0; i < targets.length; i++) {

			var target = targets[i];

			if (target.antDeployCommand) {

				if (target.bypassChanges) {
					 
					var pathToFile = target.dirPath,
						dir;

					if (pathToFile.includes("/")) {

						dir = pathToFile.slice(0, pathToFile.lastIndexOf("/"));
					} else {

						self.build.fail('Salesforce files need to be in a sub-directory.', '');
					}
 
					try {

							shell.cp('-r', paths.pathToRepo + dir, paths.deployDir);

						} catch (err) {

							self.build.fail('Not able to copy ' + paths.pathToRepo + dir + ' content to ' + paths.deployDir + ' because of the following:', err.stack);
						}
				} else if (target.changes[0].value.length > 0) {

					target.filesPaths.push(target.dirPath);

					for (j = 0; j < target.filesPaths.length; j++) {

						var pathToFile = target.filesPaths[j];
						var dir = pathToFile.slice(0, pathToFile.lastIndexOf("/"));

						var targetDir = paths.deployDir + dir,
							targetPathToFile = paths.deployDir + pathToFile,
							sourcePathToFile = paths.pathToRepo + pathToFile;

						try {

							// Creating the target directory if not here yet.
							if (!shell.test('-d', targetDir)) {

								/*****/
								/**/self.log.debug('Making directory because inexistant.');
								/*****/

								shell.mkdir('-p', targetDir);
							}

							// Copy current file to deploy target directory if not already there.
							if (!shell.test('-f', targetPathToFile)) {

								shell.cp('-r', sourcePathToFile, targetDir);
							}
	
							// Determine what the linked file would be.
							// SF xml meta file or not.
							var targetPathToLinkedFile,
								sourcePathToLinkedFile;

							if (pathToFile.includes('-meta.xml')) {

								targetPathToLinkedFile = targetPathToFile.replace('-meta.xml', '');
								sourcePathToLinkedFile = sourcePathToFile.replace('-meta.xml', '');
							} else {

								targetPathToLinkedFile = targetPathToFile + '-meta.xml';
								sourcePathToLinkedFile = sourcePathToFile + '-meta.xml';
							}

							// Copy current linked file to deploy target directory if it exists in the source and is not already in the target.
							if (!shell.test('-f', targetPathToLinkedFile) && shell.test('-f', sourcePathToLinkedFile)) {

								shell.cp('-r', sourcePathToLinkedFile, targetDir);
							}

							/*****/
							/**/self.log.debug(
							/**/	'targetDir = ' + targetDir +
							/**/	'\ntargetPathToFile = ' + targetPathToFile +
							/**/	'\nsourcePathToFile = ' + sourcePathToFile +
							/**/	'\ntargetPathToLinkedFile = ' + targetPathToLinkedFile +
							/**/	'\nsourcePathToLinkedFile = ' + sourcePathToLinkedFile
							/**/);
							/*****/	

						} catch (err) {

							self.build.fail('Not able to copy ' + paths.filesPaths[j] + ' to ' + paths.deployDir + dir + ' because of the following:', err.stack);
						}
					}	
				}			
			}
		}


		/*****/
		/**/self.log.debug('Exiting setDeployDirectory');
		/*****/

		callback();
	},

	deploy: function(i, retryCount, callback) {

		var self = this;

		/*****/
		/**/self.log.debug('Entering deploy');
		/*****/

		var targets = self.getTargets(),
			paths = self.getGlobalPaths(),
			deploySettings = self.getDeploySettings();
		
		if  (i >= targets.length) {

			callback();
		} else {

			if (targets[i].antDeployCommand) {

				console.log(
					'*** FORCE_DEPLOY_ALL = ' + targets[i].bypassChanges + ' for ' + targets[i].antDeployCommand + ' *** \n' +
					'*** BLOCK_DEPLOY = ' + targets[i].blockDeploy + ' for ' + targets[i].antDeployCommand + ' ***'
				);
				
				if (!targets[i].blockDeploy && (targets[i].bypassChanges || targets[i].changes[0].value.length > 0)) {

					try {

						shell.rm('-rf', 'logs/' + '*');
						shell.exec("echo 'Initialize log...' | tee -a logs/log", {silent:true});
					} catch (err) {

						self.build.fail('Not able to empty logs directory because of the following:', err.stack);
					}
				
					var id,
						pendingCount,
						logContent,
						path = './logs/log',
						retry = false,
						canceled = false;

					var monitor = setInterval(function() {
						try {

							logContent = fs.readFileSync(path,'utf8');
						} catch (err) {

							self.build.fail('Read log failed because of the following:', err.stack);
						}

						if (id == null) {

							var searchId = 'Request ID for the current deploy task: ';
							
							if (logContent.indexOf(searchId) != -1) {

								var posId = logContent.indexOf(searchId) + searchId.length;
								id = logContent.substring(posId, posId + 18);
							}
						}
						
						pendingCount = (logContent.match(/Request Status: Pending/g) || []).length;
						
						retry = pendingCount > parseInt(deploySettings.pendingCountMax);

						/*****/
						/**/self.log.debug(
						/**/	'Monitoring Pending Count Max = ' + deploySettings.pendingCountMax +
						/**/	'\nMonitoring Pending Count = ' + pendingCount +
						/**/	'\nMonitoring Retry = ' + retry
						/**/);
						/*****/

						if (retry && id != null && !canceled) {
							/*****/
							/**/self.log.debug('Monitoring fetched id = ' + id);
							/*****/

							console.log('***** Reached the MAXIMUM PENDING ROWS LIMIT *****');
							canceled = true;
							shell.exec('ant -Dsf.username=' + deploySettings.sfUsername + ' -Dsf.password=' + deploySettings.sfPassword + ' -Dsf.serverurl=' + deploySettings.sfServerUrl + ' -Dsf.deployRequestId=' + id + ' cancel -f ' + paths.deployDir, {silent: true}, function(code, output) {});
						}
					}, parseInt(deploySettings.buildMonitorInterval));

					console.log(
						'*** ANT COMMAND:' +
						'\n*** ant -Dsf.username=' + deploySettings.sfUsername + ' -Dsf.password=' + self.maskString(deploySettings.sfPassword) + ' -Dsf.serverurl=' + deploySettings.sfServerUrl + ' ' + targets[i].antDeployCommand
					);

					shell.exec('ant -Dsf.username=' + deploySettings.sfUsername + ' -Dsf.password=' + deploySettings.sfPassword + ' -Dsf.serverurl=' + deploySettings.sfServerUrl + ' ' + targets[i].antDeployCommand + ' -f ' + paths.deployDir + ' | tee -a logs/log', function(code, stdout, stderr) {
						
						clearInterval(monitor);

						/*****/
						/**/self.log.debug(
						/**/	'Retry of build execution = ' + retry +
						/**/	'\nBuild execution code = ' + code +
						/**/	'\nBuild execution output = ' + stdout +
						/**/	'\nBuild execution stderr = ' + stderr
						/**/);
						/*****/

						if (retry) {

							if(retryCount < parseInt(deploySettings.buildMaxRetries)) {

								console.log('***** RETRY ' + (retryCount + 1) + '/' + deploySettings.buildMaxRetries + ' *****');
								self.deploy(i, retryCount + 1, function() {
									callback();
								});
							} else {

								self.build.fail('***** Reached the MAXIMUM RETRY LIMIT (' + deploySettings.buildMaxRetries + '), ABORDING. *****', '');
							}
						} else if (stderr) {

							self.build.fail('', '');
						} else {

							self.deploy(i + 1, 0, function() {
								callback()
							});
						}
					});

					console.log('******');
				} else {

					console.log('*** NO CHANGES for ' + targets[i].antDeployCommand + ' *** \n' +
                        '******');

					self.deploy(i + 1, 0, function() {
						callback();
					});
				}
			} else {

				self.deploy(i + 1, 0, function() {
					callback();
				});
			}	
		}
	},

	setGitHead: function(callback) {

		var self = this;

		/*****/
		/**/self.log.debug('Entering setGitHead');
		/*****/

		var paths = self.getGlobalPaths(),
			deploySettings = self.getDeploySettings(),
			targets = self.getTargets();

		/*****/
		/**/self.log.debug(
		/**/	'paths.commitRefDir = ' + paths.commitRefDir +
		/**/	'\npaths.pathToRepo = ' + paths.pathToRepo
		/**/);
		/*****/

		var head = paths.commitRef;

		if (head.old == head.new) {

			/*****/
			/**/self.log.debug('Previous and current commit are the same, no update to mySQL.');
			/*****/			
			// If the previous and current commit hashes are the same, we do not need to update the mySQL db.
			callback();
		}

		for (i = 0; i < targets.length; i++) {

			if (targets[i].antDeployCommand) {

				if (targets[i].antDeployCommand.includes('-Dsf.checkOnly="true"') || targets[i].blockDeploy == true) {

					/*****/
					/**/self.log.debug('CheckOnly or blockDeploy, no update to mySQL.');
					/*****/
					// If any ant command is checkOnly OR is explicitely BLOCKED,
					// we do NOT want to write the new commit hash since not deployed.
					callback();
				}
			}
		}

		if (deploySettings.sfOrg) {

			//TEMPORARY FIX, NEED TO REFACTOR THIS (TK-2555).
			mysqldb.updateLatestCommitData(deploySettings.sfOrg, head.new, function(results) {

				/*****/
				/**/self.log.debug('updateLatestCommitData results = ' + JSON.stringify(results));
				/*****/

				if (results.warningCount > 0) {

					self.build.fail('There was an issue updating the mySQL database with latest commit:', results.message);
				}
				
				callback();
			});	
		} else {

			// Creating the folder if not here yet.
			if (!shell.test('-d', paths.commitRefDir)) {

				/*****/
				/**/self.log.debug('Making directory because inexistant.');
				/*****/

				shell.mkdir('-p', paths.commitRefDir);
			}

			try {

				// Removing remaining HEAD from last run.
				shell.rm('-rf', paths.commitRefDir + '*');

				shell.cp('-r', paths.pathToRepo + '.git/HEAD', paths.commitRefDir);
				shell.cp('-r', paths.pathToRepo + '.git/refs', paths.commitRefDir);
			} catch (err) {

				self.build.fail('Not able to perform operations from ' + paths.pathToRepo + ' to ' + paths.commitRefDir + ' because of the following:', err.stack);
			}

			/*****/
			/**/self.log.debug('Exiting setGitHead');
			/*****/

			callback();
		}
	}
}

octopus.init();
